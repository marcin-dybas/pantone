import os
import json
import datetime
from pathlib import Path
from urllib.request import urlopen
from bs4 import BeautifulSoup
from PIL import Image, ImageDraw


def saveRawSoup(raw, table):
    """
    puts the soup on the table. (saves the raw soup to a file.)
    """
    with open(table, "w", encoding="utf-8") as file:
        file.write(str(raw))


def getRawSoup(table):
    """
    downloads the raw soup and passes to the table
    """
    page_url = (
        "http://store.pantone.com/eu/en/color-of-the-day"
    )
    raw = urlopen(page_url).read().decode("utf-8")
    saveRawSoup(raw, table)
    return raw


def getPrettySoup(real):
    """
    returns prettified soup as string
    """
    soup = real.prettify()
    return soup


def getRealSoup(raw):
    """
    returns bs4 object
    raw is string or an open() file handler
    """
    soup = BeautifulSoup(raw, "html.parser")
    return soup


def getFreshSoup(table):
    """
    looks at the table and makes fresh soup from raw soup
    """
    bowl = Path(table)
    if bowl.is_file():
        currentDay = datetime.date.today()
        dayModified = datetime.date.fromtimestamp(
            os.path.getmtime(bowl)
        )
        if currentDay == dayModified:
            with open(bowl, "r") as f:
                raw = f
                return getRealSoup(raw)
        else:
            return getRealSoup(getRawSoup(table))
    else:
        return getRealSoup(getRawSoup(table))


def is_tag(self, soup):
    tag = soup.find("html")
    return isinstance(self, type(tag))


def drawRect(color):
    im = Image.new("RGB", (512, 512), color)
    im.save("fresh_color.png", "PNG")


def getPantoneSoup(fresh):
    d = {}

    color_info = [
        i.string
        for i in fresh.find(
            class_="color-content"
        ).find_all("span")
    ]
    color_keywords = [
        i.string
        for i in fresh.find(
            class_="color-keywords"
        ).contents
        if not is_tag(i, fresh)
    ]
    color_hex = [
        fresh.find(class_="color-block").get("style")[-8:-1]
    ]

    d["code"] = color_info[0]
    d["name"] = color_info[1]
    d["hex"] = color_hex[0]
    d["keywords"] = color_keywords

    drawRect(color_hex[0])

    with open("payload.json", "r") as f:
        payload = json.load(f)

        payload[0]["fields"][0]["text"] = (
            "Color of the Day: _*" + d["name"] + "*_"
        )
        
        payload[2]["fields"][0]["text"] = (
            "PANTONE® " + d["code"]
        )

        payload[2]["fields"][2]["text"] = "HEX: " + d["hex"]
        payload[2]["fields"][1]["text"] = d["keywords"][0]
        payload[2]["fields"][3]["text"] = d["keywords"][1]
        payload[2]["fields"][5]["text"] = d["keywords"][2]
        payload[2]["accessory"]["image_url"] = "https://www.colorhexa.com/" + d["hex"][1:] + ".png"

        fresh_payload = json.dumps(payload)

    return fresh_payload
