import os
import json
import requests
import datetime
from pantonesoup import getFreshSoup, getPantoneSoup
from pathlib import Path

WEBHOOK_TOKEN = os.environ["WEBHOOK_TOKEN"]

table = "raw_soup.html"
status = "status.json"

soup = getFreshSoup(table)
payload = getPantoneSoup(soup)


def isSent(status):
    status = Path(status)

    if status.is_file():

        with open(status, "r") as f:
            data = json.load(f)
        currentDay = str(datetime.date.today())

        if currentDay == data["lastResponse"]:
            print(
                "I don't like SPAM. Last soup was on {}.Timestamp: {}".format(
                    data["lastResponse"],
                    data["lastResponseTimestamp"],
                )
            )
            return True
        else:
            print(
                "Was last sent on {}. Timestamp: {}".format(
                    data["lastResponse"],
                    data["lastResponseTimestamp"],
                )
            )
            return False


def requestResponse(contentType):
    response = requests.post(
        WEBHOOK_TOKEN,
        json=({"blocks": payload}),
        headers={"Content-Type": contentType},
    )

    if response.status_code != 200:
        raise ValueError(
            "Request to slack returned an error %s, the response is:\n%s"
            % (response.status_code, response.text)
        )

    else:
        data = {}
        data["lastResponse"] = str(datetime.date.today())
        data["lastResponseTimestamp"] = str(
            datetime.datetime.now().timestamp()
        )

        with open(status, "w") as f:
            json.dump(data, f)

if not isSent(status):
    requestResponse("application/json")
